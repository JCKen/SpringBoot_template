package com.railway.procurementsystem.entity;

import com.github.pagehelper.Page;
import com.railway.procurementsystem.base.BaseModel;
import com.railway.procurementsystem.tool.UuidTool;
import lombok.Data;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table
@Data
public class EntityModel extends BaseModel {

    @Id  //字符串主键
    @KeySql(genId = UuidTool.class)
    private String uuid;
    /*数据库字段*/
    private String name;

    /*@Id  //数字主键
    @KeySql(useGeneratedKeys = true)
    private Integer numId;*/

    /*临时属性*/
    @Transient
    private Object obj;








    /*分页属性*/
    @Transient
    private Integer page;
    @Transient
    private Integer limit;
    public Integer getPage() {
        if(page==null||page<1){
            page=1;
        }
        return page;
    }
    public Integer getLimit() {
        if(limit==null||limit<1){
            limit=10;
        }
        return limit;
    }

}
