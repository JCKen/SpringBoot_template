package com.railway.procurementsystem.dao;

import com.railway.procurementsystem.base.BaseMapper;
import com.railway.procurementsystem.entity.EntityModel;

/*BaseMapper< 实体类,主键类型> */
public interface EntityModelDao extends BaseMapper< EntityModel,String> {}
