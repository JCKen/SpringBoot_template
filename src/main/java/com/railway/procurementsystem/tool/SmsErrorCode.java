package com.railway.procurementsystem.tool;


import java.util.HashMap;
import java.util.Map;

public class SmsErrorCode {
    public static Map<String,String> error = new HashMap<>();
    static {
        error.put("isv.BLACK_KEY_CONTROL_LIMIT", "黑名单管控");
        error.put("isv.MOBILE_NUMBER_ILLEGAL", "非法手机号");
        error.put("VALVE:D_MC", "重复过滤");
        error.put("VALVE:H_MC", "重复过滤");
        error.put("VALVE:M_MC", "重复过滤");
        error.put("isv.ACCOUNT_ABNORMAL", "账户异常");
        error.put("isv.AMOUNT_NOT_ENOUGH", "账户余额不足");
        error.put("isv.ACCOUNT_NOT_EXISTS", "账户不存在");
        error.put("isp.SYSTEM_ERROR", "系统错误");
        error.put("isv.SMS_SIGNATURE_ILLEGAL", "短信签名不合法");
        error.put("isv.SMS_TEMPLATE_ILLEGAL", "短信模板不合法");
        error.put("isv.TEMPLATE_MISSING_PARAMETERS", "模板缺少变量");
        error.put("isv.TEMPLATE_PARAMS_ILLEGAL", "模板变量里包含非法关键字");
        error.put("isv.PRODUCT_UN_SUBSCRIPT", "未开通云通信产品的阿里云客户");
        error.put("isv.MOBILE_COUNT_OVER_LIMIT", "手机号码数量超过限制");
        error.put("isv.PARAM_LENGTH_LIMIT", "参数超出长度限制");
        error.put("isv.INVALID_PARAMETERS", "参数异常");
        error.put("FILTER", "关键字拦截");
        error.put("isv.PRODUCT_UNSUBSCRIBE", "产品未开通");
        error.put("isv.BUSINESS_LIMIT_CONTROL", "业务限流");
        error.put("isv.OUT_OF_SERVICE", "业务停机");
        error.put("isv.PARAM_NOT_SUPPORT_URL", "不支持URL");
        error.put("isp.RAM_PERMISSION_DENY", "RAM权限DENY");
        error.put("isv.INVALID_JSON_PARAM", "JSON参数不合法，只接受字符串值");
    }
}
