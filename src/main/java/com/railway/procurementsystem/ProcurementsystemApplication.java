package com.railway.procurementsystem;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@MapperScan("com.railway.procurementsystem.dao")
public class ProcurementsystemApplication {

    public static void main ( String[] args ) {
        SpringApplication.run(ProcurementsystemApplication.class, args);
    }

}
