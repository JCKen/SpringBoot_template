package com.railway.procurementsystem.base;

import com.railway.procurementsystem.biz.impl.manager.ServiceManager;

import javax.annotation.Resource;

public class BaseService {

    @Resource
    protected ServiceManager serviceManager;

    protected String likeField(String str){
        return "%"+str+"%";
    }

}
