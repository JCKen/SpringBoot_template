package com.railway.procurementsystem.tool;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.Random;

@Slf4j
public class SendSMS {
    /*私有AK帐号*/
    private static final String accessKeyId = "LTAIAS7M3295efuC";
    private static final String accessKeySecret = "74Aayf2ZLG270B5DEJCRWtdY7FkQ5V";

    /*场景*/
    private static String sceneNo;
    private static final String signName = "广铁商城";
    private static final String registerNo = "SMS_171858780";
    private static final String loginNo = "SMS_171853769";
    private static final String rePwdNo = "SMS_171858781";

    /*验证码*/
    private static int num;
    private static Random random = new Random();



    public static boolean send ( String scene, Integer randomNum, String phone) {
        boolean state = false;
        switch (scene) {
            case "register":
                sceneNo = registerNo;
                break;
            case "login":
                sceneNo = loginNo;
                break;
            case "rePwd":
                sceneNo = rePwdNo;
                break;
        }
        if (StringUtils.isBlank(sceneNo))
            return state;
        /*验证码*/
        if (randomNum == null || randomNum.toString()=="") {
            num = random.nextInt(1000000) + 100000;
        } else {
            num = randomNum;
        }
        //设置超时时间-可自行调整
        System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
        System.setProperty("sun.net.client.defaultReadTimeout", "10000");
        //初始化ascClient需要的几个参数
        final String product = "Dysmsapi";//短信API产品名称（短信产品名固定，无需修改）
        final String domain = "dysmsapi.aliyuncs.com";//短信API产品域名（接口地址固定，无需修改）
        //初始化ascClient,暂时不支持多region（请勿修改）
        IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
        try {
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        IAcsClient acsClient = new DefaultAcsClient(profile);
        //组装请求对象
        SendSmsRequest request = new SendSmsRequest();
        //使用post提交
        request.setMethod(MethodType.POST);
        //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式；发送国际/港澳台消息时，接收号码格式为国际区号+号码，如“85200000000”
        request.setPhoneNumbers(phone);
        //必填:短信签名-可在短信控制台中找到
        request.setSignName(signName);
        //必填:短信模板-可在短信控制台中找到，发送国际/港澳台消息时，请使用国际/港澳台短信模版
        request.setTemplateCode(sceneNo);
        //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
        //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
        request.setTemplateParam("{\"code\":" + num + "}");
        //可选-上行短信扩展码(扩展码字段控制在7位或以下，无特殊需求用户请忽略此字段)
        //request.setSmsUpExtendCode("90997");
        //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
        //request.setOutId("yourOutId");
        //请求失败这里会抛ClientException异常
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = acsClient.getAcsResponse(request);
        } catch (ClientException e) {
            e.printStackTrace();
        }
        String code = sendSmsResponse.getCode();
        if (code != null && code.equals("OK")) {
            //请求成功
            state = true;
            log.debug("场景：" + scene + ",手机号：" + phone + "," + "短信发送请求成功");
        } else {
            state = false;
            String result = "场景：" + scene + ",手机号：" + phone + "," + "短信发送请求失败！ " + "失败代码：" + code + ",原因：" + SmsErrorCode.error.get(code);
            log.debug(result);
        }
        return state;
    }

    public static void main ( String[] args ) {
        String result = null;
        send("login", null, "18820115845");
    }
}
