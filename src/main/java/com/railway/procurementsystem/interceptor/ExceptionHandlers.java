package com.railway.procurementsystem.interceptor;

import com.alibaba.fastjson.JSON;
import com.railway.procurementsystem.base.ResultBean;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ExceptionHandlers {

    @ExceptionHandler(Exception.class)
    public ResultBean exceptionHandler( Exception e, HttpServletRequest request) {
        String time = new SimpleDateFormat("yyyy-MM-dd  HH:mm:ss:SSS").format(System.currentTimeMillis());
        log.error("\n******************************  发生异常  "+time+" ******************************************");
        String requestURI = request.getRequestURI();
        Map<String, String[]> args = request.getParameterMap();
        String msg = " ==> 请求的url: " + requestURI + "\n ==> 请求的参数: " + JSON.toJSONString(args);
        log.error(msg);
        log.error("异常原因: "+ e.getMessage());
        log.error(e.getMessage(),e);
        log.error("************************************************************************");
        return ResultBean.fail("参数异常!"+msg);
    }


}
