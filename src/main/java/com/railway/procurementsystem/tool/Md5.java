package com.railway.procurementsystem.tool;

import org.apache.commons.codec.digest.DigestUtils;

public class Md5 {
    private static final String salt1 = "dhsighw";
    private static final String salt2 = "xingasf";

    public static String  Md5(String sac){
        //commons-lang3  version:3.6
        String md5 =  DigestUtils.md5Hex(sac);
        return md5;
    }

    /**
     * 第一次明文加固定salt加密
     * @param inputPass
     * @return
     */
    public static String inputPassToFormPass(String inputPass){
        String str = ""+salt1.charAt(0)+salt1.charAt(2) + inputPass +salt1.charAt(5) + salt1.charAt(4);
        return Md5(str);

    }

    /**
     * 第二次加密md5的值
     */
    public static String formPassToDBPass(String formPass){
        String str  = ""+salt2.charAt(0)+salt2.charAt(2) + formPass +salt2.charAt(5) + salt2.charAt(4);
        return Md5(str);
    }

    /**
     * 整体加密
     *
     */
    public static String getEncryption(String pass){
        //第一次加密
        String onePass = inputPassToFormPass(pass);
        //第二次加密
        return formPassToDBPass(onePass);
    }

    public static void main ( String[] args ) {
        String pwd =  Md5.getEncryption("111111");
        System.out.println(pwd.length());
        System.out.println(pwd);

        String pwd1 =  Md5.getEncryption("222222");
        System.out.println(pwd1.length());
        System.out.println(pwd1);
    }
}