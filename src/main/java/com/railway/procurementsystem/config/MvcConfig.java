package com.railway.procurementsystem.config;

import com.railway.procurementsystem.interceptor.LogInterceptor;
import com.railway.procurementsystem.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Resource
    private LoginInterceptor interceptor;

    @Resource
    private LogInterceptor logInterceptor;


    /*配制拦截器*/
    @Override
    public void addInterceptors ( InterceptorRegistry registry ) {
        registry.addInterceptor(interceptor)
                //指定要拦截的请求 /** 表示拦截所有请求
                .addPathPatterns("/**")
                //排除不需要拦截的请求路径
                .excludePathPatterns("/api/user/login/**","/api/user/register");

        registry.addInterceptor(logInterceptor).addPathPatterns("/**");
    }

}
