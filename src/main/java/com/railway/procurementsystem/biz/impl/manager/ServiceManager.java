package com.railway.procurementsystem.biz.impl.manager;

import com.railway.procurementsystem.biz.EntityModelBiz;
import com.railway.procurementsystem.tool.RedisUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service("serviceManager")
public class ServiceManager {

    /*这个类全部写BIZ接口的对象，方便集中管理调用*/

    @Resource
    public RedisUtil redisUtil;

    @Resource(name = "entityModelBiz")
    public EntityModelBiz entityModelBizImpl;


}
