package com.railway.procurementsystem.biz.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.railway.procurementsystem.base.BaseService;
import com.railway.procurementsystem.biz.EntityModelBiz;
import com.railway.procurementsystem.biz.impl.manager.ServiceManager;
import com.railway.procurementsystem.dao.EntityModelDao;
import com.railway.procurementsystem.entity.EntityModel;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;

@Service("entityModelBiz")
public class EntityModelBizImpl extends BaseService implements EntityModelBiz {

    @Resource
    private EntityModelDao entityModelDao;
    @Resource
    private ServiceManager serviceManager;

    @Override
    public void addEntity ( EntityModel entityModel ) {
        entityModelDao.insert(entityModel);
        serviceManager.redisUtil.set("allEntityModels", null);
    }

    @Override
    public void updateEntity ( EntityModel entityModel ) {
        entityModelDao.updateByPrimaryKeySelective(entityModel);
    }

    @Override
    public void delEntity ( String id ) {
        entityModelDao.deleteByPrimaryKey(id);
        serviceManager.redisUtil.set("allEntityModels", null);
    }

    @Override
    public void addEntityList ( List< EntityModel > list ) {
        entityModelDao.insertList(list);
        serviceManager.redisUtil.set("allEntityModels", null);
    }

    @Override
    public void delEntityList ( List< String > ids ) {
        entityModelDao.deleteByIdList(ids);
        serviceManager.redisUtil.set("allEntityModels", null);
    }

    @Override
    public EntityModel getById ( String id ) {
        return entityModelDao.selectByPrimaryKey(id);
    }

    @Override
    public List< EntityModel > getAll ( ) {
        List<EntityModel> allEntityModels = (List< EntityModel >) serviceManager.redisUtil.get("allEntityModels");
        if(CollectionUtils.isEmpty(allEntityModels)){
            allEntityModels = entityModelDao.selectAll();
            serviceManager.redisUtil.set("allEntityModels", allEntityModels);
        }
        return allEntityModels;
    }

    @Override
    public List< EntityModel > getListByField ( Object obj, String orderBy, boolean isAsc ) {
        EntityModel entityModel = (EntityModel) obj;
        Example e = new Example(EntityModel.class);
        Example.Criteria criteria = e.createCriteria();
        if(StringUtils.isNotBlank(entityModel.getUuid())){
            criteria.andEqualTo("id", entityModel.getUuid());
        }
        if(StringUtils.isNotBlank(entityModel.getName())){
            criteria.andLike("name", likeField(entityModel.getName()));
        }

        if(StringUtils.isNotBlank(orderBy)){
            if(isAsc){
                e.orderBy(orderBy).asc();
            }else {
                e.orderBy(orderBy).desc();
            }
        }

        List< EntityModel > list = entityModelDao.selectByExample(e);
        return list;
    }

    @Override
    public EntityModel getOneByField ( Object obj ) {
        EntityModel entityModel = (EntityModel) obj;
        Example e = new Example(EntityModel.class);
        Example.Criteria criteria = e.createCriteria();
        if(StringUtils.isNotBlank(entityModel.getUuid())){
            criteria.andEqualTo("id", entityModel.getUuid());
        }
        if(StringUtils.isNotBlank(entityModel.getName())){
            criteria.andLike("name", likeField(entityModel.getName()));
        }

        EntityModel model = entityModelDao.selectOneByExample(e);
        return model;
    }

    @Override
    public PageInfo< EntityModel > getListWithPage ( Object obj, String orderBy, boolean isAsc ) {
        EntityModel entityModel = (EntityModel) obj;
        PageHelper.startPage(entityModel.getPage(),entityModel.getLimit());
        List< EntityModel > list = getListByField(entityModel, null, false);
        PageInfo< EntityModel > p = new PageInfo<>(list);
        return p;
    }
}
