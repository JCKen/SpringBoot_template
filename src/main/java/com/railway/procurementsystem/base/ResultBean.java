package com.railway.procurementsystem.base;

import javax.swing.filechooser.FileView;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ResultBean {

    public static final String SUCCESS = "success";
    public static final String ERROR = "error";
    public static final boolean True = true;
    public static final boolean False = false;

    private Long count;

    private String status = ERROR;

    private boolean result = False;

    private String msg;

    private String requestToken;

    private Object[] objs;

    private Object data;

    public String getRequestToken ( ) {
        return requestToken;
    }

    public void setRequestToken ( String requestToken ) {
        this.requestToken = requestToken;
    }

    public String getStatus ( ) {
        return status;
    }

    public void setStatus ( String status ) {
        this.status = status;
    }

    public String getMsg ( ) {
        return msg;
    }

    public void setMsg ( String msg ) {
        this.msg = msg;
    }

    public Object[] getObjs ( ) {
        return objs;
    }

    public void setObjs ( Object... objs ) {
        this.objs = objs;
    }

    public Object getData ( ) {
        return data;
    }

    public void setData ( Object data ) {
        this.data = data;
    }

    public Long getCount ( ) {
        return count;
    }

    public void setCount ( Long count ) {
        this.count = count;
    }

    //设置附件信息
    public void setFileData ( List< FileView > files ) {
        Map< String, Object > datas = new HashMap< String, Object >();
        datas.put("files", files);
        this.data = datas;
    }

    public boolean isResult ( ) {
        return result;
    }

    public void setResult ( boolean result ) {
        this.result = result;
    }

    public ResultBean ( ) {

    }

    public ResultBean ( String msg, boolean result, Object data, Long count ) {
        this.msg = msg;
        this.result = result;
        this.data = data;
        this.count = count;
    }

    public ResultBean ( String msg, boolean result, Object data ) {
        this.msg = msg;
        this.result = result;
        this.data = data;
    }

    public ResultBean ( String msg, boolean result ) {
        this.msg = msg;
        this.result = result;
    }

    public ResultBean ( boolean result ) {
        this.result = result;
    }

    public ResultBean ( boolean result, Object data ) {
        this.result = result;
        this.data = data;
    }

    public static ResultBean success ( ) {return new ResultBean("操作成功！", True);}

    public static ResultBean success ( String msg ) {return new ResultBean(msg, True);}

    public static ResultBean success ( String msg , Object data) {return new ResultBean(msg, True,data);}

    public static ResultBean success ( Object data ) {return new ResultBean("操作成功！", True, data);}

    public static ResultBean fail ( ) {return new ResultBean("操作失败！", False);}

    public static ResultBean fail ( String msg ) {return new ResultBean(msg, False);}

    public static ResultBean fail ( Object data ) {return new ResultBean("操作失败！", False, data);}

    public static ResultBean success ( Object data, Long count ) {return new ResultBean("操作成功！", True, data, count);}

    public static ResultBean fail ( Object data, Long count ) {return new ResultBean("操作失败！", False, data, count);}


}