package com.railway.procurementsystem.tool;


import tk.mybatis.mapper.genid.GenId;

import java.util.UUID;
public class UuidTool implements GenId<String> {


    @Override
    public String genId ( String table, String column ) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        return  uuid;
    }
}
