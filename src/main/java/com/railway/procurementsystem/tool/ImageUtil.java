package com.railway.procurementsystem.tool;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;

/**
 * @author 聂鹏
 * @description:
 * @create 2019/05/24
 */
public final class ImageUtil {

	/**
	 * 验证码字符集
	 * 去除 l、o、I、O、0、1
	 * 目的避免混淆
	 */
	private static final char[] chars = "abcdefghjkmnpqrstuvwxyz23456789".toCharArray();
	// 字符数量
	private static final int SIZE = 4;
	// 干扰线数量
	private static final int LINES = 10;
	// 图片宽度
	private static final int WIDTH = 120;
	// 图片高度
	private static final int HEIGHT = 45;
	// 字体大小
	private static final int FONT_SIZE = 30;

	/**
	 * 生成随机验证码及图片
	 * 数组中[验证码，图片]
	 */
	public static Object[] createImage() {
		StringBuffer sb = new StringBuffer();
		// 1.创建空白图片
		BufferedImage image = new BufferedImage(
			WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
		// 2.获取图片画笔
		Graphics graphic = image.getGraphics();
		// 3.设置画笔颜色
		graphic.setColor(Color.LIGHT_GRAY);
		// 4.绘制矩形背景
		graphic.fillRect(0, 0, WIDTH, HEIGHT);
		// 5.画随机字符
		Random ran = new Random();
		for (int i = 0; i <SIZE; i++) {
			// 取随机字符索引
			int n = ran.nextInt(chars.length);
			// 设置字体随机颜色
			graphic.setColor(getRandomColor());
			// 设置字体大小
			graphic.setFont(new Font(
				null, Font.BOLD + Font.ITALIC, FONT_SIZE));
			// 画字符,x:后面的10是设置第一个字的左边距,y:后面的10是设置字的上边距
			graphic.drawString(
				chars[n] + "", i * WIDTH / (SIZE+1)+10, HEIGHT / 2+10);
			// 记录字符
			sb.append(chars[n]);
		}
		// 6.画干扰线
		for (int i = 0; i < LINES; i++) {

			graphic.setColor(getRandomColor());
			// 随机画线
			graphic.drawLine(ran.nextInt(WIDTH), ran.nextInt(HEIGHT),
				ran.nextInt(WIDTH), ran.nextInt(HEIGHT));
		}
		// 7.返回验证码和图片
		return new Object[]{sb.toString(), image};
	}

	/**
	 * 随机取色
	 */
	public static Color getRandomColor() {
		Random ran = new Random();
		Color color = new Color(ran.nextInt(256),
			ran.nextInt(256), ran.nextInt(256));
		return color;
	}
	//测试生成图片
	public static void main(String[] args) throws IOException {
		Object[] objs = createImage();
		BufferedImage image = (BufferedImage) objs[1];
		OutputStream os = new FileOutputStream("src/main/resources/1.png");
		ImageIO.write(image, "png", os);
		os.close();
	}

}
