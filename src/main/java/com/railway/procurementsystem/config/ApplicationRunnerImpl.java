package com.railway.procurementsystem.config;

import com.railway.procurementsystem.biz.impl.manager.ServiceManager;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;


/*项目启动后执行的方法*/
@Component
public class ApplicationRunnerImpl implements ApplicationRunner {

    @Resource
    private ServiceManager serviceManager;

    @Override
    public void run ( ApplicationArguments args ) throws Exception {
        /*项目启动后清除全部redis缓存*/
        serviceManager.redisUtil.flushAll();
        System.err.println("项目启动成功！☺");
    }
}
