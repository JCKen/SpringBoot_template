package com.railway.procurementsystem.interceptor;

import com.railway.procurementsystem.base.BaseController;
import com.railway.procurementsystem.biz.impl.manager.ServiceManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
@Component
public class LoginInterceptor extends BaseController implements HandlerInterceptor {

    @Resource
    private ServiceManager serviceManager;

    //定义一个线程域，存放登录的对象
    //private static final ThreadLocal<User> thread = new ThreadLocal<>();

    @Override
    public boolean preHandle ( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception {
        //设置编码格式
        //        String queryString = request.getQueryString();
        //        System.err.println("请求参数: "+queryString);
        //        response.setCharacterEncoding("UTF-8");
        //        response.setContentType("application/json;charset=UTF-8");
        //        String id = request.getHeader("id");
        //        if(id==null) id = (String) serviceManager.redisUtil.get("id");
        //        if(id==null){
        //            response.getWriter().write(JSON.toJSONString(ResultBean.fail("无权访问!")));
        //            return false;
        //        }
        //        User sessionUser = (User) serviceManager.redisUtil.get(id);
        //        if(sessionUser==null){
        //            response.getWriter().write(JSON.toJSONString(ResultBean.fail("未登录!!")));
        //            return false;
        //        }
        //        String uri = request.getRequestURI();
        //        if(sessionUser.getRoleId()!=1){
        //            List<Power> powers = serviceManager.powerBizImpl.getAll();
        //            Map<String,Integer> urls = new HashMap();
        //            for(Power power : powers){
        //                urls.put(power.getUrl(), power.getId());
        //            }
        //
        //            Integer urlId = urls.get(uri);
        //            String userPower = (String) serviceManager.redisUtil.get("userPower"+sessionUser.getId());

        //            System.err.println("powers: "+JSON.toJSONString(powers));
        //            System.err.println("uri: "+JSON.toJSONString(uri));
        //            System.err.println("urls: "+JSON.toJSONString(urls));
        //            System.err.println("urlId: "+JSON.toJSONString(urlId));
        //            System.err.println("userPower: "+JSON.toJSONString(userPower));

        //            if(urlId!=null){
        //                if(!userPower.contains(urlId.toString())){
        //                    response.getWriter().write(JSON.toJSONString(ResultBean.fail("权限不够!")));
        //                    return false;
        //                }
        //            }
        //        }
        //        thread.set(sessionUser);
        return true;
    }

    @Override
    public void postHandle ( HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView ) throws Exception {

    }

    @Override
    public void afterCompletion ( HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex ) throws Exception {
        //        User user = getUser();
        //        //重新设置redis过期时间
        //        serviceManager.redisUtil.set(user.getId(),user,(60*30));
        //        //过滤器完成后，从线程域中删除用户信息
        //        thread.remove();
    }


    //获取登陆用户
    //    public static User getUser() {
    //        return thread.get();
    //    }


    @Override
    protected void setData ( ) {

    }
}
