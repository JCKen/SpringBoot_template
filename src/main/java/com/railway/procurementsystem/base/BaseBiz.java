package com.railway.procurementsystem.base;

import com.github.pagehelper.PageInfo;

import java.util.List;

public interface BaseBiz< T extends Object, TK extends Object > {

    void addEntity(T t);
    void updateEntity(T t);
    void delEntity(TK id);

    void addEntityList(List<T> list);
    void delEntityList(List<TK> ids);


    T getById ( TK id );

    List<T> getAll();

    /*通过字段查找集合对象*/
    List< T > getListByField ( Object obj, String orderBy, boolean isAsc );

    /*通过字段查找集合对象*/
    T getOneByField ( Object obj );

    PageInfo< T > getListWithPage ( Object obj, String orderBy, boolean isAsc );
}
