package com.railway.procurementsystem.base;

import java.io.Serializable;

public abstract class BaseModel implements Serializable {

    Integer page;
    Integer limit;

    public Integer getPage() {
        if(page==null||page<1){
            page=1;
        }
        return page;
    }

    public Integer getLimit() {
        if(limit==null||limit<1){
            limit=10;
        }
        return limit;
    }


}
