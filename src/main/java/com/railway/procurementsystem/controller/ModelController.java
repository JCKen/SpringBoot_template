package com.railway.procurementsystem.controller;

import com.railway.procurementsystem.base.ResultBean;
import com.railway.procurementsystem.biz.impl.manager.ServiceManager;
import com.railway.procurementsystem.entity.EntityModel;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/api/model/")
public class ModelController {
    @Resource
    private ServiceManager serviceManager;

    @PostMapping("addModel")
    public ResultBean addModel( @RequestBody EntityModel entityModel ){
        serviceManager.entityModelBizImpl.addEntity(entityModel);
        return ResultBean.success("添加成功");
    }

    @RequestMapping("getAll")
    public ResultBean getAll(){
        List< EntityModel > list = serviceManager.entityModelBizImpl.getAll();
        return ResultBean.success(list);
    }
}
