package com.railway.procurementsystem.base;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;


@Slf4j
public abstract class BaseController {
    protected static final String MESSAGE = "message";

    protected HttpServletRequest request;

    protected HttpServletResponse response;

    protected ServletContext servletContext;

    protected WebApplicationContext applicationContext;

    protected String requestRootUrl;

    protected String getMessage ( String code ) {

        return this.getMessage(code, new Object[]{});
    }

    protected String getMessage ( String code, Object arg0 ) {

        return this.getMessage(code, new Object[]{arg0});
    }

    protected String getMessage ( String code, Object arg0, Object arg1 ) {

        return this.getMessage(code, new Object[]{arg0, arg1});
    }

    protected String getMessage ( String code, Object arg0, Object arg1, Object arg2 ) {

        return this.getMessage(code, new Object[]{arg0, arg1, arg2});
    }

    protected String getMessage ( String code, Object arg0, Object arg1, Object arg2, Object arg3 ) {

        return this.getMessage(code, new Object[]{arg0, arg1, arg2, arg3});
    }

    protected String getMessage ( String code, Object[] args ) {

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        Locale locale = localeResolver.resolveLocale(request);
        return this.applicationContext.getMessage(code, args, locale);
    }

    public void setServletContext ( ServletContext servletContext ) {
        this.servletContext = servletContext;
        this.applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(servletContext);
    }

    /**
     * 获取客户端的真实IP地址
     */
    protected String getIpAddress ( HttpServletRequest request ) {
        final String unknown = "unknown";
        String ip = request.getHeader("x-forwarded-for");
        if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
            ip = request.getHeader("Proxy-Client-IP");
        if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
            ip = request.getHeader("WL-Proxy-Client-IP");
        if (StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip))
            ip = request.getRemoteAddr();
        return ip;
    }

    /**
     * 获取当前登录用户的信息
     */
//    protected User getSessionUser ( ) {
//        User sessionUser = null;
//        try {
//            User user = (User) request.getSession().getAttribute("user");
//            if (user != null) {
//                sessionUser = user;
//            }
//        } catch (Exception e) {
//            catchError(null, e);
//        }
//        return sessionUser;
//    }

//    protected User getSessionUser (HttpServletRequest request) {
//        User sessionUser = null;
//        try {
//            User user = (User) request.getSession().getAttribute("user");
//            if (user != null) {
//                sessionUser = user;
//            }
//        } catch (Exception e) {
//            catchError(null, e);
//        }
//        return sessionUser;
//    }

//    protected Boolean isLogin ( ) {
//        if (getSessionUser() != null) {
//            return true;
//        }
//        return false;
//    }

    /**
     * 自动注入当前Request对象
     *
     * @param request
     */
    @ModelAttribute
    public void setReq ( HttpServletRequest request, HttpServletResponse response ) {
        this.request = request;
        this.response = response;
//        request.setAttribute("userAndPower", getSessionUser());
        setData();
    }


    protected ResultBean validateErrors ( BindingResult errors ) {
        ResultBean result = new ResultBean();
        String message = "";
        if (errors.hasErrors()) {
            for (ObjectError objectError : errors.getAllErrors()) {
                message = this.getMessage(objectError.getCode(), objectError.getArguments());
                result.setMsg(message);
                return result;
            }
        }
        return null;
    }

    protected void catchError ( ResultBean resultBean, Exception e) {
        if (resultBean != null) {
            resultBean.setStatus(ResultBean.ERROR);
            resultBean.setMsg(e.getMessage());
        }
        log.debug("请求发生错误！！！");
        log.debug("错误原因： " + e);
        e.printStackTrace();

    }

    protected void catchError ( Exception e ,PrintWriter out,String result) {
        if(out!=null){
            out.println(false);
            out.print(result);
        }
        log.debug("请求发生错误！！！");
        log.debug("错误原因： " + e);
        e.printStackTrace();

    }

    protected PrintWriter getPrintWriter ( ) throws IOException {
        PrintWriter out = response.getWriter();
        return out;
    }

    protected void close ( PrintWriter out ) {
        out.flush();
        out.close();
    }

    protected  void setData ( ){}



}
