package com.railway.procurementsystem.interceptor;

import com.railway.procurementsystem.base.BaseController;
import com.railway.procurementsystem.biz.impl.manager.ServiceManager;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.NamedThreadLocal;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Map;

@Slf4j
@Component
public class LogInterceptor extends BaseController implements HandlerInterceptor {

    @Resource
    private ServiceManager serviceManager;

    @Override
    public boolean preHandle ( HttpServletRequest request, HttpServletResponse response, Object handler ) throws Exception {
//        long startTime = System.currentTimeMillis();
//        String ipAddress = getIpAddress(request);
//        String id = (String) serviceManager.redisUtil.get("id");
//        User user = (User) serviceManager.redisUtil.get(id);
//        request.setAttribute("startTime", startTime);
//        startTimeThreadLocal.set(startTime);		//线程绑定变量（该数据只有当前请求的线程可见）
//        if (handler instanceof HandlerMethod) {
//            StringBuilder sb = new StringBuilder(1000);
//            sb.append("-----------------------开始计时:").append(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(startTime)).append("-------------------------------------\n");
//            HandlerMethod h = (HandlerMethod) handler;
//            sb.append("请求用户ID: ").append(user==null?null:user.getId()).append("\n");
//            sb.append("请求用户IP地址: ").append(ipAddress==null?null:ipAddress).append("\n");
//            sb.append("被请求Controller: ").append(h.getBean().getClass().getName()).append("\n");
//            sb.append("被请求方法: ").append(h.getMethod().getName()).append("\n");
//            sb.append("请求参数: ").append(getParamString(request.getParameterMap())).append("\n");
//            sb.append("请求的URI: ").append(request.getRequestURI()).append("\n");
//            log.debug(sb.toString());
//        }
        return true;
    }

    @Override
    public void postHandle ( HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView ) throws Exception {

    }

    @Override
    public void afterCompletion ( HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex ) throws Exception {
//        if (log.isDebugEnabled()) {
//            long beginTime = startTimeThreadLocal.get();//得到线程绑定的局部变量（开始时间）
//            long endTime = System.currentTimeMillis();    //2、结束时间
//
//            log.debug("---计时结束：" + new SimpleDateFormat("HH:mm:ss.SSS").format(endTime)
//                    + " 耗时: " + (endTime-beginTime) +" ms;  "
//                    + " URI: " + request.getRequestURI()
//                    + ";   最大内存: " + Runtime.getRuntime().maxMemory() / 1024 / 1024
//                    + "m;    已分配内存: " + Runtime.getRuntime().totalMemory() / 1024 / 1024
//                    + "m;    已分配内存中的剩余空间: " + Runtime.getRuntime().freeMemory() / 1024 / 1024
//                    + "m;    最大可用内存: " + (Runtime.getRuntime().maxMemory() - Runtime.getRuntime().totalMemory()
//                    + Runtime.getRuntime().freeMemory()) / 1024 / 1024 + "m"+"\n");
//            startTimeThreadLocal.remove();
//        }


    }


    private static final ThreadLocal< Long > startTimeThreadLocal =
            new NamedThreadLocal< Long >("ThreadLocal StartTime");

    private String getParamString(Map<String,String[]> map){

        StringBuilder sb = new StringBuilder();
        for (Map.Entry< String, String[] > e : map.entrySet()) {
            sb.append(e.getKey()).append("=");
            String[] value = e.getValue();
            if (value != null && value.length == 1) {
                sb.append(value[0]).append("\t");
            } else {
                sb.append(Arrays.toString(value)).append("\t");
            }
        }
        return sb.toString();
    }

    public static String getStackTraceAsString(Throwable e) {
        if (e == null){
            return "";
        }
        StringWriter stringWriter = new StringWriter();
        e.printStackTrace(new PrintWriter(stringWriter));
        return stringWriter.toString();
    }


    @Override
    protected void setData ( ) {

    }
}
