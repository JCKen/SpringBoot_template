package com.railway.procurementsystem.biz;

import com.railway.procurementsystem.base.BaseBiz;
import com.railway.procurementsystem.entity.EntityModel;

/*BaseBiz< 实体类,主键类型> */
public interface EntityModelBiz extends BaseBiz< EntityModel,String> {}
